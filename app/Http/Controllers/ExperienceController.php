<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\experience;
use Illuminate\Support\Facades\Session;

class ExperienceController extends Controller
{
    public function index() 
    {
        $experience = experience::all();

        return view('hiring.index', compact('experience'));
    }

    public function store(Request $request)
    {
    	try {
    		$experience = new experience();
            $experience->name = $request->name;
            $experience->organizer = $request->organizer;
            $experience->year = $request->year;
    		$experience->location = $request->location;
    		$experience->save();

    		Session::flash('message', 'Data Berhasil Disimpan');
    		return redirect()->back();
    	}catch(Exception $e) {
    		Session::flash('message', 'Data Gagal Disimpan');
    		return redirect()->back();
    	}
    }

	public function delete($id)
    {
        
        $experience = experience::find($id);
        $experience->delete();
        
        Session::flash('message', 'Berhasil menghapus');
        return redirect()->back();
    }
}
