<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\hiring;
use App\Models\lastedu;
use App\Models\experience;
use App\Models\work;
use Illuminate\Support\Facades\Session; 

class HiringController extends Controller
{
    public function index() 
    {
        $lastedu = lastedu::all();
        $hiring = hiring::all();

        return view('hiring.index', compact('hiring', 'lastedu'));
    }

    public function store(Request $request)
    {
    	try {
    		$imageName = time().'.'.request()->image->getClientOriginalExtension();
    		request()->image->move(public_path('image'), $imageName);

    		$hiring = new hiring();
            $hiring->name = $request->name;
            $hiring->address = $request->address;
            $hiring->phone = $request->phone;
    		$hiring->email = $request->email;
            $hiring->dateofbirth = $request->dateofbirth;
            $hiring->religion = $request->religion;
            $hiring->edustage = $request->edustage;
            $hiring->lastedu = $request->lastedu;
            $hiring->photo = $imageName;
    		$hiring->save();

    		Session::flash('message', 'Data Berhasil Disimpan');
    		return redirect()->back();
    	}catch(Exception $e) {
    		Session::flash('message', 'Data Gagal Disimpan');
    		return redirect()->back();
    	}
    }

    public function add_experience(Request $request)
    {
        
    }
    
    public function delete($id)
    {
        
        $hiring = hiring::find($id);
        $hiring->delete();
        
        Session::flash('message', 'Berhasil menghapus');
        return redirect()->back();
    }

}
