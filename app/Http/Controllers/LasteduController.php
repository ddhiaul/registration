<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\lastedu;
use Illuminate\Support\Facades\Session;

class LasteduController extends Controller
{
    public function index() 
    {
        $lastedu = lastedu::all();

        return view('lastedu.index', compact('lastedu'));
    }

    public function store(Request $request)
    {
    	$this->validate($request,[
    		'edustage' => 'required' 
    	]);

    	try {
    		$lastedu = new lastedu();
    		$lastedu->edustage = $request->edustage;
    		$lastedu->save();

    		Session::flash('message', 'Data Berhasil Disimpan');
    		return redirect()->back();
    	} catch (Exception $e) {
    		Session::flash('message', 'Data Tidak Berhasil Disimpan');
    		return redirect()->back();
    	}
	}
	
	public function delete($id)
    {
        
        $lastedu = lastedu::find($id);
        $lastedu->delete();
        
        Session::flash('message', 'Berhasil menghapus');
        return redirect()->back();
	}
}
