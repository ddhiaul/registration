<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\work;
use Illuminate\Support\Facades\Session;

class WorkController extends Controller
{
    public function index() 
    {
        $work = work::all();

        return view('hiring.index', compact('work'));
    }

    public function store(Request $request)
    {
    	try {
    		$work = new work();
            $work->company = $request->company;
            $work->address = $request->address;
            $work->field = $request->field;
            $work->position = $request->position;
            $work->year = $request->year;
    		$work->save();

    		Session::flash('message', 'Data Berhasil Disimpan');
    		return redirect()->back();
    	} catch (Exception $e) {
    		Session::flash('message', 'Data Tidak Berhasil Disimpan');
    		return redirect()->back();
    	}
	}
	
	public function delete($id)
    {
        
        $work = work::find($id);
        $work->delete();
        
        Session::flash('message', 'Berhasil menghapus');
        return redirect()->back();
	}
}
