<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class experience extends Model
{
    protected $table = "experience";

    public function hiring()
    {
        return $this->belongsTo('App\Models\hiring');
    }
}
