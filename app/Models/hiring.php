<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class hiring extends Model
{
    protected $table = "hiring";

    public function lastedu()
    {
        return $this->hasOne('App\Models\lastedu');
    }

    public function experience()
    {
        return $this->hasMany('App\Models\experience');
    }

    public function work()
    {
        return $this->hasMany('App\Models\work');
    }
}
