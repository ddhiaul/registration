<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class work extends Model
{
    protected $table = "work";

    public function hiring()
    {
        return $this->belongsTo('App\Models\hiring');
    }
}
