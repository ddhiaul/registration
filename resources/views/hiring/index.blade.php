@include('base.header')

<div class="content-wrapper">
    <div class="row">
        <div class="offset col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="row">
                <div class="col-md-12"> 
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="mb-0">Registration</h4>
                            </div>
                            <div class="card-body">
                                <form class="needs-validation" novalidate="">
                                    <div class="row">
                                        <div class="col-md-12 mb-3">
                                            <label>Name</label>
                                            <input type="text" class="form-control" placeholder="John Doe">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 mb-3">
                                            <label>Address</label>
                                            <input type="text" class="form-control" placeholder="City, Country">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 mb-3">
                                            <label>Phone</label>
                                            <input type="text" class="form-control" placeholder="08xx-xxxx-xxxx">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 mb-3">
                                            <label>Email</label>
                                            <input type="text" class="form-control" placeholder="example@gmail.com">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 mb-3">
                                            <label>Date of Birth</label>
                                            <input type="text" class="form-control" placeholder="DD MM YYYY">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 mb-3">
                                            <label>Religion</label>
                                            <input type="text" class="form-control" placeholder="Religion">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 mb-3">
                                            <label>Last Education</label>
                                            <select class="custom-select d-block w-100">
                                                <option value="">Choose...</option>
                                            </select>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <input type="text" class="form-control" placeholder="School Name">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 mb-3">
                                            <label>Your Photo</label>
                                            <input type="file" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                    <hr class="mb-4">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="mb-0">Seminar/Training/Courses Experiences</h4>
                                        </div>
                                        <div class="card-body">
                                            <form class="needs-validation" novalidate="">
                                                <div class="row">
                                                    <div class="col-md-12 mb-3">
                                                        <label>Seminar/Training/Courses Name</label>
                                                        <input type="text" class="form-control" placeholder="Name">
                                                        <div class="mb-md-12 mb-3"></div>
                                                        <label>Organizer</label>
                                                        <input type="text" class="form-control" placeholder="Organizer">
                                                        <div class="mb-md-12 mb-3"></div>
                                                        <label>Year</label>
                                                        <input type="text" class="form-control" placeholder="YYYY">
                                                        <div class="mb-md-12 mb-3"></div>
                                                        <label>Location</label>
                                                        <input type="text" class="form-control" placeholder="Building, City">
                                                        <div class="mb-md-12 mb-3"></div>
                                                        <div class="mb-md-12 mb-3">
                                                            <button class="btn btn-success btn-md" type="submit">Add</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr class="mb-4">
                                                <div class="col-xl-12 ">
                                                    <div class="card">
                                                        <h5 class="card-header">Experiences Table</h5>
                                                        <div class="card-body p-0">
                                                            <form action="/registration" method="GET">
                                                                <div class="table-responsive">
                                                                    <table class="table">
                                                                        <thead class="bg-light">
                                                                            <tr class="border-0">
                                                                                <th class="border-0">#</th>
                                                                                <th class="border-0">Name</th>
                                                                                <th class="border-0">Organizer</th>
                                                                                <th class="border-0">Year</th>
                                                                                <th class="border-0">Location</th>
                                                                                <th class="border-0"></th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>1</td>
                                                                                <td>Name</td>
                                                                                <td>Organizer</td>
                                                                                <td>Year</td>
                                                                                <td>Location</td>
                                                                                <td>
                                                                                    <a class="btn btn-danger btn-sm" href="">Delete</a>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <hr class="mb-4">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="mb-0">Job Experiences</h4>
                                        </div>
                                        <div class="card-body">
                                            <form class="needs-validation" novalidate="">
                                                <div class="row">
                                                    <div class="col-md-12 mb-3">
                                                        <label>Company Name</label>
                                                        <input type="text" class="form-control" placeholder="Company Name">
                                                        <div class="mb-md-12 mb-3"></div>
                                                        <label>Address</label>
                                                        <input type="text" class="form-control" placeholder="Address">
                                                        <div class="mb-md-12 mb-3"></div>
                                                        <label>Field of Work</label>
                                                        <input type="text" class="form-control" placeholder="Field of Work">
                                                        <div class="mb-md-12 mb-3"></div>
                                                        <label>Position</label>
                                                        <input type="text" class="form-control" placeholder="Position">
                                                        <div class="mb-md-12 mb-3"></div>
                                                        <label>Year</label>
                                                        <input type="text" class="form-control" placeholder="YYYY - YYYY">
                                                        <div class="mb-md-12 mb-3"></div>
                                                        <div class="mb-md-12 mb-3">
                                                            <button class="btn btn-success btn-md" type="submit">Add</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr class="mb-4">
                                                <div class="col-xl-12 ">
                                                    <div class="card">
                                                        <h5 class="card-header">Experiences Table</h5>
                                                        <div class="card-body p-0">
                                                            <form action="/registration" method="GET">
                                                                <div class="table-responsive">
                                                                    <table class="table">
                                                                        <thead class="bg-light">
                                                                            <tr class="border-0">
                                                                                <th class="border-0">#</th>
                                                                                <th class="border-0">Company Name</th>
                                                                                <th class="border-0">Address</th>
                                                                                <th class="border-0">Field of Work</th>
                                                                                <th class="border-0">Position</th>
                                                                                <th class="border-0">Year</th>
                                                                                <th class="border-0"></th>
                                                                            </tr>
                                                                        </thead>
                                                                        @foreach($work as $item)
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>{{ $item->id }}</td>
                                                                                <td>{{ $item->company }}</td>
                                                                                <td>{{ $item->address }}</td>
                                                                                <td>{{ $item->field }}</td>
                                                                                <td>{{ $item->position }}</td>
                                                                                <td>{{ $item->year }}</td>
                                                                                <td>
                                                                                    <a class="btn btn-danger btn-sm" href="">Delete</a>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                        @endforeach
                                                                    </table>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <button class="btn btn-primary btn-lg btn-block" type="submit">submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('base.footer')