@include('base.header')

<div class="content-wrapper">
    <div class="row">
        <div class="offset col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="row">
                <div class="col-md-12"> 
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="mb-0">Educational Stage</h4>
                            </div>
                            <div class="card-body">
                                <form class="needs-validation" novalidate="" action="/edustage" method="POST">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-12 mb-3">
                                            <label>Name</label>
                                            <input type="text" class="form-control" placeholder="Enter Name...">
                                            <div class="mb-md-12 mb-3"></div>
                                            <button class="btn btn-primary btn-lg " type="submit">add</button>
                                        </div>
                                    
                                    </div>
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="mb-0">Educational Stage Table</h4>
                                            <div class="card-body p-0">
                                                <form action="/edustage" method="GET">
                                                    <div class="table-responsive">
                                                        <table class="table">
                                                            <thead class="bg-light">
                                                                <tr class="border-0">
                                                                    <th class="border-0">#</th>
                                                                    <th class="border-0">Name</th>
                                                                    <th class="border-0"></th>
                                                                </tr>
                                                            </thead>
                                                            @foreach($lastedu as $item)
                                                            <tbody>
                                                                <tr>
                                                                    <td>{{ $item->id }}</td>
                                                                    <td>{{ $item->name }}</td>
                                                                    <td>
                                                                        <a class="btn btn-danger btn-sm" href="">Delete</a>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                            @endforeach
                                                        </table>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('base.footer')