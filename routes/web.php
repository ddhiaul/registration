<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('template');
});

Route::get('registrant', 'App\Http\Controllers\HiringController@index');

//registration
Route::get('registration', 'App\Http\Controllers\HiringController@index');
Route::get('registration', 'App\Http\Controllers\ExperienceController@index');
Route::get('registration', 'App\Http\Controllers\WorkController@index');

Route::post('registration', 'App\Http\Controllers\HiringController@store');
Route::post('registration', 'App\Http\Controllers\ExperienceController@store');
Route::post('registration', 'App\Http\Controllers\WorkController@store');

//eduStage
Route::post('edustage', 'App\Http\Controllers\LasteduController@store');
Route::get('edustage', 'App\Http\Controllers\LasteduController@index');